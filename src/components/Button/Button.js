import React from 'react';
import './Button.scss';
import classNames from 'classnames';

export const Button = ({ children, color, defaultMargin }) => {
  const classes = classNames('button', `button--${color || 'default'}`, {
    'button--default-margin': defaultMargin,
  });

  return <button className={classes}>{children || 'Button'}</button>;
};
