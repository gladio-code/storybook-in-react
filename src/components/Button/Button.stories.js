import React from 'react';
import { Button } from './Button';
import { withKnobs, text } from '@storybook/addon-knobs';

export default {
  title: 'Button',
  component: Button,
  decorators: [withKnobs],
};

export const basic = () => <Button>{text('Button text', 'Click me!')}</Button>;

export const Blue = () => <Button color="blue">Blue</Button>;

export const red = () => <Button color="red">red</Button>;
