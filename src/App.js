import React from 'react';
import { Button } from './components/Button/Button';
import './App.scss';

function App() {
  return (
    <div className="app">
      <Button></Button>
    </div>
  );
}

export default App;
